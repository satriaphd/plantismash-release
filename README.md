antiSMASH - the antibiotics and Secondary Metabolite Analysis SHell
===================================================================

[![Build Status](https://drone.io/bitbucket.org/antismash/antismash/status.png)](https://drone.io/bitbucket.org/antismash/antismash/latest)

antiSMASH allows the rapid genome-wide identification, annotation and analysis
of secondary metabolite biosynthesis gene clusters in bacterial and fungal
genomes. It integrates and cross-links with a large number of in silico
secondary metabolite analysis tools that have been published earlier.

External tools
--------------

antiSMASH is powered by several open source tools: NCBI BLAST+,HMMer 3, Muscle
3, Glimmer 3, FastTree, TreeGraph 2, Indigo-depict, PySVG and JQuery SVG.

The database additionally requires PostgreSQL and psycopg2

Development & Funding
---------------------

The development of antiSMASH was started as a collaboration of the Department
of Microbial Physiology and Groningen Bioinformatics Centre of the University
of Groningen, the Department of Microbiology of the University of Tübingen, and
the Department of Bioengineering and Therapeutic Sciences at the University of
California, San Francisco.
With the move of the PIs and developers, development continues now at the
Manchester Institute of Biotechnology, the Bioinformatics Group at Wageningen
University and The Novo Nordisk Foundation Center for Biosustainability in
Hørsholm.

antiSMASH development was/is supported by the GenBiotics program of the Dutch Technology
Foundation (STW), which is the applied-science division of The Netherlands
Organisation for Scientific Research (NWO) and the Technology Programme of the
Ministry of Economic Affairs (grant STW 10463), GenBioCom program
of the German Ministry of Education and Research (BMBF) grant 0315585A, the
German Center for Infection Research (DZIF) and the Novo Nordisk Foundation.

Publications
------------

Kai Blin, Marnix H. Medema, Daniyal Kazempour, Michael A. Fischbach, Rainer
Breitling, Eriko Takano, & Tilmann Weber (2013): antiSMASH 2.0 — a versatile
platform for genome mining of secondary metabolite producers. Nucleic Acids
Research 41: W204-W212.

Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager, Piotr
Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer Breitling & Eriko
Takano (2011). antiSMASH: Rapid identification, annotation and analysis of
secondary metabolite biosynthesis gene clusters. Nucleic Acids Research 39:
W339-W346.


Installation
------------

Unfortunately, antiSMASH is a bit complicated to install from source, and we
recommend using one of the pre-built installers from
[our download page](http://antismash.secondarymetabolites.org/download)
instead.

###Manual installation on Linux

First, make sure you have the following antiSMASH dependencies installed:

- [glimmer](https://ccb.jhu.edu/software/glimmer/) (version 3.02 tested)
- [GlimmerHMM](https://ccb.jhu.edu/software/glimmerhmm/) (version 3.0.4 tested)
- [hmmer2](http://hmmer.janelia.org/download.html) (version 2.3.2 tested, append a 2 to all hmmer2 executables to avoid conflict with hmmer3 executable names, like hmmalign -> hmmalign2)
- [hmmer3](http://hmmer.janelia.org/download.html) (version 3.0 and 3.1b2 tested)
- [fasttree](http://www.microbesonline.org/fasttree/#Install) (version 2.1.7 tested)
- [diamond](https://github.com/bbuchfink/diamond) (> 0.7.9, version 0.7.10 tested)
- [muscle](http://www.drive5.com/muscle/downloads.htm) (version 3.8.31 tested)
- [prodigal](http://prodigal.ornl.gov/) (version 2.6.1 tested)
- [NCBI blast+](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/) (version 2.2.31 tested)
- [xz](http://tukaani.org/xz/) development headers (version 5.1.1 tested)
- [xml](http://xmlsoft.org) development headers (version 2.9.1 tested)
- python (version 2.7 tested, anything >= python 2.6 should work)
- python-virtualenv (not needed, but highly recommended)

> Note that DIAMOND 0.7.10 has a bug causing it to always exit with an error
> status, even if it ran successfully. This in turn causes antiSMASH to log an
> error message. This is harmless. Still, we recommend updating to DIAMOND 0.7.11
> once that becomes available.

Then, create a python virtualenv for installing the antiSMASH python
dependencies. This is not required, but highly recommended.

```bash
virtualenv as3
source as3/bin/activate
```

All the python dependencies are listed in `requirements.txt`, you can grab them
all and install them with a simple command:

```bash
pip install -r requirements.txt
```

Last but not least, run `download_databases.py` to grab and prepare the
databases:

```bash
python download_databases.py
```

###Database setup guide

_Note: the antiSMASH database integration is likely to change in future versions
and still considered experimental_.

1. Install PostgreSQL according to your operation system's specifications
2. Install python db and psycopg2 module
	pip install db
	pip install psycopg2

2. Setup BioSQL database
	Owner: biosql
	Database name: biosql
	Password (standard): biosql # should be changed in config/default.cfg

	2a: Follow the guide http://biopython.org/wiki/BioSQL; load taxonomy using the supplied perl script (requires Bioperl/Bioperl-DB)
	2b: Alternatively load biosqldump.sql
	(download from https://bitbucket.org/tilmweber/antismash-db_private/downloads); this include BioSQL with pre-loaded NCBI taxonomy database

3. If you wish to use the nosetest framework also for testing the database modules, setup a second BioSQL database. This database is used only for the test scripts
	Owner: biosql
	Database name: antiSMASHnosetest
	Password: biosql
